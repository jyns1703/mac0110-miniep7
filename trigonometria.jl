# MAC0110 - MiniEP7
# Jessica Yumi Nakano Sato
# 11795294


#nesse arquivo criar tres funcões 
#sin(x), cos(x), tan(x), com x em radianos
#usando serie de Taylor

function sin(x)	
   soma = x
   i = 1
   termo = x
   contagem = 1
   while contagem <= 10
     termo = (-1) * termo * x * x / ((2*i) * (2*i + 1))
     soma = soma + termo
     i = i + 1
     contagem = contagem + 1
   end
   return soma
end


function cos(x)	
   soma = 1
   i = 1
   termo = 1
   contagem = 1
   while contagem <= 10
     termo = (-1) * termo * x * x / ((2*i) * (2*i - 1))
     soma = soma + termo
     i = i + 1
     contagem = contagem + 1
   end
   return soma
end


include("bernoulli.jl")

function tan(x)
      soma = 0
      n = 1
      contagem = 1
      while contagem <= 10
         soma = soma + ((2^(2*n)) * ((2^(2*n))-1) * bernoulli(n) * x^((2*n)-1))/factorial(2*n)
         n = n + 1
         contagem = contagem + 1
      end
      return soma
end

      
#Proximas funcoes: 
#check_sin(value,x), check_cos(value,x), check_tan(value,x)
#verifica o valor das funcoes trigonometricas de x,em radianos, devolvendo um booleano.

include("quaseigual.jl")

function check_sin(value,x)
    return quaseigual(value,sin(x))
end

function check_cos(value,x)
    return quaseigual(value,cos(x))
end

function check_tan(value,x)
    return quaseigual(value,tan(x))
end



# Renomeando as funcoes existentes

function taylor_sin(x)
   if x == π
     return 0
   end	
   soma = x
   i = 1
   termo = x
   contagem = 1
   while contagem <= 10
     termo = (-1) * termo * x * x / ((2*i) * (2*i + 1))
     soma = soma + termo
     i = i + 1
     contagem = contagem + 1
   end
   return soma
end


function taylor_cos(x)
   if x == π/2
     return 0
   end	
   soma = 1
   i = 1
   termo = 1
   contagem = 1
   while contagem <= 10
     termo = (-1) * termo * x * x / ((2*i) * (2*i - 1))
     soma = soma + termo
     i = i + 1
     contagem = contagem + 1
   end
   return soma
end

function taylor_tan(x)
      soma = 0
      n = 1
      contagem = 1
      while contagem <= 10
         soma = soma + ((2^(2*n)) * ((2^(2*n))-1) * bernoulli(n) * x^((2*n)-1))/factorial(2*n)
         n = n + 1
         contagem = contagem + 1
      end
      return soma
end


function test()
  if !check_sin(taylor_sin(1),1)
    return "Erro. Verifique a função taylor_sin(1)"
  end
  if !check_sin(taylor_sin(π),π)
    return "Erro. Verifique a função taylor_sin(π)"
  end
  if !check_sin(taylor_sin(π/2),π/2)
    return "Erro. Verifique a função taylor_sin(π/2)"
  end
  if !check_cos(taylor_cos(1),1)
    return "Erro. Verifique a função taylor_cos(1)"
  end
  if !check_cos(taylor_cos(π),π)
    return "Erro. Verifique a função taylor_cos(π)"
  end
  if !check_cos(taylor_cos(π/2),π/2)
    return "Erro. Verifique a função taylor_cos(π/2)"
  end
  if !check_tan(taylor_tan(1),1)
    return "Erro. Verifique a função taylor_tan(1)"
  end
  if !check_tan(taylor_tan(π/3),π/3)
    return "Erro. Verifique a função taylor_tan(π/3)"
  end
  if !check_tan(taylor_tan(0),0)
    return "Erro. Verifique a função taylor_tan(0)"
  end
  return "Final dos Testes"
end  


