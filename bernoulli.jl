#calcula o numero de Bernoulli para ser usado na funcao tangente do arquivo trigonometria.jl

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
       A[m+1] = 1 // (m+1)
       for j = m : -1 : 1
           A[j] = j * (A[j] - A[j+1])
       end
    end
    return abs(A[1])
end 
